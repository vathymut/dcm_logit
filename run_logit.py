# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 01:35:39 2013

@author: Vathy M. Kamulete
"""
# %%
import pandas as pd
import numpy as np
import scipy.optimize as opt

# Import own modules
from os.path import abspath, dirname
import sys
from __future__ import division
# Add path to look into
# print __file__
# sys.path.insert( 0, abspath( dirname(__file__) ) )
import logit as lg

# %%
# Load dataset
df = pd.read_csv( 'datalong.csv' )
# Columns of interest
idcase = df.idcase.values
idalt = df.idalt.values
iddep = df.depvar.values
depvars = df[['ic', 'oc']].values

# %%
# Set up global variables to functions to estimate the logit model.
d = lg.setup_logit( depvars, iddep, idcase, idalt )
estimate_logit = d[ 'estimate_logit' ]

# %%
# Initiate random coefficients
no_coefs = depvars.shape[1]
coef_rand = np.random.randn(no_coefs)
# The correct coefficients
coef_corr = [-0.00623187, -0.00458008]

# %%                                          
#Test finding optimal coefficients - no intercpets 
opt.fmin( estimate_logit, coef_corr )

# %%
# with ranadom coefficients
opt.fmin( estimate_logit, coef_rand )

# %%
# Calculate shares                     
calc_shares = d[ 'calc_shares' ] 
pred_probs_logit = d[ 'pred_probs_logit' ]
pred_probs = pred_probs_logit( coef_corr )
calc_shares( pred_probs )

# %%
# with random coefficients and bfgs algorithm
opt.fmin_bfgs( estimate_logit, 
               coef_rand,
               full_output = False )
               
# %%
# with random coefficients and bfgs algorithm
opt.fmin_bfgs( estimate_logit, 
               coef_rand,
               full_output = True )
      
# %%          
# Test finding optimal coefficients - with intercpets 
add_intercepts_logit = d[ 'add_intercepts_logit' ]   
# Add intercepts
depvars_with_int = add_intercepts_logit( data = depvars )
# Initiate random coefficients
no_coefs = depvars_with_int.shape[1]
coef_rand_ints = np.random.randn(no_coefs)
# Set up functions to estimate logit.
d = lg.setup_logit( depvars_with_int, iddep, idcase, idalt )
estimate_logit = d[ 'estimate_logit' ]

# %%
# with intercepts and random coefficients and bfgs algorithm
opt.fmin( estimate_logit, 
          coef_rand_ints,
          maxfun = 1000000000,
          full_output = False )

# %%
# Save results
res = opt.fmin_bfgs( estimate_logit, 
                     coef_rand_ints, 
                     retall = True,
                     full_output = True )
        




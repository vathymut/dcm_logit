# README

Python Implementation of Ken Train's MATLAB codes for his course Economics 244 (based on the textbook 'Discrete Choice Methods with Simulation). 
Click on link [here](http://elsa.berkeley.edu/users/train/ec244ps.html) for more info.

## Problem sets
The code, so far, covers only the first problem set.

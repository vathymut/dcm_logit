# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 16:41:26 2013
@author: Vathy M. Kamulete

This code is a Python implementation of Kenneth Train Matlab codes.

Definitions of terms used below :
-------------------------------------------------------------------------------
ncs:    float
    number of choice situations    
idcase: array_like (NROWS, )
    NROWSx1 vector identifying the rows for each choice situation
    Rows with same (unique) id denotes the same
    agent/household/person choosing among the alternatives.
unique_ids: array_like (ncs, )
    NCSx1 vector of the unique idcase
    
iddep:  array_like (NROWS, )
    NROWSx1 vector identifying the chosen alternative 
    ( 1=chosen, 0=nonchosen )
iddep_mask: array_like (NROWS, ) 
    boolean indicating chosen options (i.e iddep == 1)

nca:    float
    number of alternatives  
idalt:  array_like (NROWS, )
    NROWSx1 vector identifying the alternatives
unique_alts: array_like (nca, )
    NCAx1 vector of the unique choice indicators
    
depvars:   array_like (NROWS, K)
    NROWSxK matrix of explanatory variables
    
coef:   array_like (K, )
    Kx1 where K is number of explanatory variables
    
pred_probs: array_like (NROWS, )
    NROWSx1 vector of predicted choice probabilities
    
loglik: float
    the log likelihood of the model
    
spl_shares: array_like (nca, )
    NCAx1 vector of sample shares for each alternative.
pred_shares:    array_like (nca, )
    NCAx1 vector of predicted shares for each alternative.
-------------------------------------------------------------------------------
    
"""

from __future__ import division
import numpy as np

def setup_logit( depvars, iddep, idcase, idalt ):
    '''
    Create variables and functions needed to estimate the logit model.
    These obviate the need to create global variables.
    Return these functions in a dictionary d:
            calc_indirect_util, calc_loglik_logit, estimate_logit,
            pred_probs_logit, calc_shares, create_intercepts_logit,
            add_intercepts_logit, scale_indirect_util
    '''
    # Variables to be used within functions below
    # Option chosen
    iddep_mask = ( iddep == 1 )
    # number of choice situations
    unique_ids = np.unique( idcase )
    ncs = np.shape( unique_ids )[0]
    # number of alternatives
    unique_alts = np.unique( idalt )
    nca = np.shape( unique_alts )[0]
    
    # Define functions needed
    def calc_indirect_util( coef ):
        '''
        Calculate the indirect utility for each option.
        '''
        # Preallocate indirect utility
        rows = np.shape( depvars )[0]
        indirect_util = np.empty( rows )
        indirect_util = np.dot( depvars, coef )
        return indirect_util
        
    def scale_indirect_util( indirect_util, 
                             alt_to_scale = None, scaled_coef = None ):
        '''
        Scale the indirect utilityfor each option.
        '''
        if scaled_coef is None or alt_to_scale is None:
            return indirect_util
        scale_coef = np.ones_like( nca )
        scale_coef[ alt_to_scale, ] = scaled_coef
        scaled_weights = np.tile( scale_coef, ncs )
        return scaled_weights*indirect_util
    
    def calc_loglik_logit( indirect_util ):
        '''
        Calculate the negative log likelihood for the logit model.
        '''
        # Preallocate choice probabilities
        prob = np.empty_like( unique_ids, dtype = 'float' )
        for idx in xrange( ncs ):
            cid_mask = ( idcase == unique_ids[idx] )
            chosen_mask = np.logical_and( cid_mask, iddep_mask )
            indirect_util_cid = indirect_util[cid_mask]-indirect_util[chosen_mask]
            prob[idx] = 1./np.sum( np.exp( indirect_util_cid ) )
        prob = np.maximum( prob, 0.0000000001 )
        loglik = np.sum( np.log( prob ) )
        # Return the negative of the loglik for optimizer
        return -1.0*loglik # could be multiplied by *( 1./ncs )
        
    def estimate_logit( coef,
                        alt_to_scale = None, 
                        scaled_coef = None ):
        '''
        Estimate the logit model coefficients.
        '''
        indirect_util = calc_indirect_util( coef = coef )
        indirect_util = scale_indirect_util( indirect_util, 
                                             alt_to_scale, scaled_coef)
        return calc_loglik_logit( indirect_util = indirect_util )
        
    def pred_probs_logit( coef ):
        '''
        Predicts probabilities for each alternative.
        '''
        # Preallocate choice probabilities
        pred_probs = np.empty_like( idcase, dtype=np.float64 )
        exp_indirect_util = np.exp( calc_indirect_util( coef = coef ) )
        for idx in xrange( ncs ):
            idcase_mask = ( idcase == unique_ids[idx] )
            num = exp_indirect_util[ idcase_mask ]
            denom = np.sum( num )
            pred_probs[ idcase_mask ] = num/denom
        return pred_probs
        
    def calc_shares( pred_probs, on_screen = True ):
        """
        Compute sample and predicted market shares.
        """
        # Preallocate sample_shares and predicted_shares
        spl_shares = np.empty( nca )
        pred_shares = np.empty_like( spl_shares )
        for idx in xrange( nca ):
            alt = unique_alts[ idx ]
            alt_mask = ( idalt == alt )
            spl_shares[idx] = np.mean( iddep[alt_mask] )
            pred_shares[idx] = np.mean( pred_probs[alt_mask] )
            if on_screen:
                print 'For Alternative: %s; ' % (alt)
                print 'Sample shares = %.2f; Predicted = %.2f' \
                % ( spl_shares[idx], pred_shares[idx] )
        return spl_shares, pred_shares, unique_alts
        
    def create_intercepts_logit( reflevel = 0 ):
        '''
        Create intercepts for each alternative.
        '''
        # Preallocate intercpets
        rows = np.shape( idalt )[0]
        intercepts = np.empty( (rows, nca) )
        
        for idx in xrange(nca):
            alt = unique_alts[ idx ]
            intercepts[ : , idx ] = np.where( idalt == alt, 1., 0. )
        # Delete column (reflevel is by default the first row)
        return np.delete( intercepts, reflevel, 1 )
        
    def add_intercepts_logit( data, reflevel = 0):
        '''
        Add intercepts to logit data.
        '''
        intercepts = create_intercepts_logit( reflevel = reflevel )
        return np.hstack( ( data, intercepts ) )

    # Return all the relevant functions
    d = {}
    d[ 'calc_indirect_util' ] = calc_indirect_util 
    d[ 'scale_indirect_util' ] = scale_indirect_util
    d[ 'calc_loglik_logit' ] = calc_loglik_logit 
    d[ 'estimate_logit' ] = estimate_logit 
    d[ 'pred_probs_logit' ] = pred_probs_logit
    d[ 'calc_shares' ] = calc_shares
    d[ 'create_intercepts_logit' ] = create_intercepts_logit 
    d[ 'add_intercepts_logit' ] = add_intercepts_logit 
    return d